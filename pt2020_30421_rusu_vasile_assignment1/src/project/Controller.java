package project;

import java.awt.event.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    private View view;
    private Model model;

    //CONSTRUCTOR
    public Controller(Model model,View view){
        this.model=model;
        this.view=view;
        view.addAdditionListener(new AdditionCommand());
        view.addSubtractionListener(new SubtractionCommand());
        view.addMultiplicationListener(new MultiplicationCommand());
        view.addDivisionListener(new DivisionCommand());
        view.addDerivativeListener(new DerivationCommand());
        view.addIntegralListener(new IntegrationCommand());
        view.addClearListener(new ClearCommand());
    }

    public static Polynom readInput(String input){
        String in=input;
        Pattern pattern=Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
        Matcher matcher=pattern.matcher(input);
        Polynom poly=new Polynom();
        while(matcher.find()){
            Monom mon=new Monom( Integer.parseInt(matcher.group(2)),(double)Integer.parseInt(matcher.group(1)));
            poly.addMonom(mon);
        }
        return poly;
    }

    class AdditionCommand implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String in1 = "";
            String in2 = "";
            Polynom result=new Polynom();
            try {
                in1 = view.getInput1();
                in2 = view.getInput2();
                result=result.Addition(readInput(in1),readInput(in2));
                view.setResult(result.printPolynom());
                view.setResult2("~NOT USED~");

            } catch (NumberFormatException nfex) {
                view.setResult("Bad Input");
            }
        }
    }
    class SubtractionCommand implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String in1 = "";
            String in2 = "";
            Polynom result=new Polynom();
            try {
                in1 = view.getInput1();
                in2 = view.getInput2();
                model.setResult(result=result.Subtraction(readInput(in1),readInput(in2)));
                view.setResult(model.getResult().printPolynom());
                view.setResult2("~NOT USED~");

            } catch (NumberFormatException nfex) {
                view.setResult("Bad Input");
            }
        }
    }
    class MultiplicationCommand implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String in1 = "";
            String in2 = "";
            Polynom result=new Polynom();
            try {
                in1 = view.getInput1();
                in2 = view.getInput2();
                model.setResult(result=result.Multiplication(readInput(in1),readInput(in2)));
                view.setResult(result.printPolynom());

            } catch (NumberFormatException nfex) {
                view.setResult("Bad Input");
                view.setResult2("~NOT USED~");
            }
        }
    }
    class DivisionCommand implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String in1 = "";
            String in2 = "";
            Polynom restP=new Polynom();
            Polynom catP=new Polynom();
            try {
                in1 = view.getInput1();
                in2 = view.getInput2();
                if(in2.equals("0"))
                    view.setResult("Can't divide by 0");
                else {
                    model.setResult(restP = restP.rest(readInput(in1), readInput(in2)));
                    model.setResult2(catP = catP.cat(readInput(in1), readInput(in2)));
                    view.setResult(catP.printPolynom());
                    view.setResult2(restP.printPolynom());
                }

            } catch (NumberFormatException nfex) {
               view.setResult("Bad Input");
            }
        }
    }
    class DerivationCommand implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String in1 = "";
            Polynom result=new Polynom();
            try {
                in1 = view.getInput1();
                model.setResult(result=result.Derivation(readInput(in1)));
                view.setResult(model.getResult().printPolynom());
                view.setResult2("~NOT USED~");
            } catch (NumberFormatException nfex) {
                view.setResult("Bad Input");
            }
        }
    }
    class IntegrationCommand implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String in1 = "";
            Polynom result=new Polynom();
            try {
                in1 = view.getInput1();
                model.setResult(result=result.Integration(readInput(in1)));
                view.setResult(model.getResult().printPolynom());
                view.setResult2("~NOT USED~");
            } catch (NumberFormatException nfex) {
                view.setResult("Bad Input");

            }
        }
    }
    class ClearCommand implements ActionListener{
        public void actionPerformed(ActionEvent e){
            model.clear();
            view.reset();
        }
    }
}
