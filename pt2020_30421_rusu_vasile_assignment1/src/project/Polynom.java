package project;

import java.util.ArrayList;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Polynom {
    private static DecimalFormat df2 = new DecimalFormat("#.##");
    private ArrayList<Monom> monomList= new ArrayList<Monom>();
    //Constructor
    public Polynom(){
    }
    //function for adding a monom
    public void addMonom(Monom m1){
        monomList.add(m1);
        arrangePolynom();
    }

    //Getter MonomList
    public ArrayList<Monom> getMonomList() {
        return monomList;
    }

    //Setter MnomoList
    public void setMonomList(ArrayList<Monom> monomList) {
        this.monomList = monomList;
    }
    public Polynom copyPolynom(Polynom p){
        for (Monom m:p.getMonomList()) {
            addMonom(m);
        }
        return this;
    }
    public int getDegree(){
        return monomList.get(0).getExponent();
    }
    public boolean isZero(){
        if (monomList.size() == 0 || (getDegree() == 0 && monomList.get(0).getCoefficient() == 0)) return true;
        return false;
    }
    public void arrangePolynom(){
        for(int i=0;i<this.getMonomList().size()-1;i++)
            for(int j=i+1;j<this.getMonomList().size();j++) {
                if (this.getMonomList().get(i).getExponent() < this.getMonomList().get(j).getExponent()) {
                    int tempExponent = this.getMonomList().get(i).getExponent();
                    double tempCoefficient = this.getMonomList().get(i).getCoefficient();
                    this.getMonomList().get(i).setExponent(this.getMonomList().get(j).getExponent());
                    this.getMonomList().get(i).setCoefficient(this.getMonomList().get(j).getCoefficient());
                    this.getMonomList().get(j).setExponent(tempExponent);
                    this.getMonomList().get(j).setCoefficient(tempCoefficient);
                }
            }
    }

    public String printPolynom( ){
        String toPrint="";
        if(monomList.size()==0)
            toPrint="null Polynom";
        else {
            for (int i = 0; i < monomList.size(); i++)
                if(monomList.get(i).getExponent()>=0) {
                    if(monomList.get(i).getCoefficient()!=0) {
                        if (i < monomList.size() - 1) {
                            if (i != 0 && monomList.get(i).getCoefficient() > 0) {
                                toPrint += "+";
                            }
                            if (monomList.get(i).getCoefficient() ==Math.floor(monomList.get(i).getCoefficient()))
                                toPrint += (int) monomList.get(i).getCoefficient();
                            else
                                toPrint += df2.format(monomList.get(i).getCoefficient());
                            toPrint += "(X^" + monomList.get(i).getExponent() + ")";
                        } else {
                            if (monomList.get(i).getCoefficient() > 0) {
                                toPrint += "+";
                            }
                            if (monomList.get(i).getCoefficient() == Math.floor(monomList.get(i).getCoefficient()))
                                toPrint += (int) monomList.get(i).getCoefficient();
                            else
                                toPrint += df2.format(monomList.get(i).getCoefficient());
                            toPrint += "(X^" + monomList.get(i).getExponent() + ")";
                        }
                    }
                }
        }
        return toPrint;
    }
    public Polynom Addition(Polynom p1,Polynom p2) {
        Polynom resultP=new Polynom();
        for (Monom i: p1.getMonomList()) {
            boolean found = false;
            Monom result = new Monom(0, 0);
            for (Monom j: p2.getMonomList()) {
                if (i.getExponent() == j.getExponent()) {
                    result.addition(i, j);
                    resultP.addMonom(result);
                    found = true;
                }
            }
            if (!found)
                resultP.addMonom(i);

        }
        for (Monom j: p2.getMonomList()) {
            boolean found = false;
            for (Monom i: p1.getMonomList()) {
                Monom result = new Monom(0, 0);
                if (i.getExponent() == j.getExponent()) {
                    found = true;
                }
            }
            if (!found)
                resultP.addMonom(j);

        }
        resultP.arrangePolynom();
        return resultP;
    }
    public Polynom Multiplication(Polynom p1,Polynom p2){
        Polynom resultP=new Polynom();
        for (Monom i: p1.getMonomList()) {
            for (Monom j : p2.getMonomList()) {
                Monom result = new Monom(0, 0);
                result.multiplication(i, j);
                boolean found = false;
                for (Monom m : resultP.getMonomList())
                    if (result.getExponent() == m.getExponent()) {
                        found = true;
                        m.setCoefficient(result.getCoefficient() + m.getCoefficient());
                    }
                if (found != true)
                        resultP.addMonom(result);
            }
        }
        return resultP;
    }
    public Polynom Subtraction(Polynom p1,Polynom p2) {
        Polynom resultP=new Polynom();
        for (Monom i: p1.getMonomList()) {
            boolean found = false;
            Monom result = new Monom(0, 0);
            for (Monom j: p2.getMonomList()) {
                if (i.getExponent() == j.getExponent()) {
                    result.subtraction(i, j);
                    resultP.addMonom(result);
                    found = true;
                }
            }
            if (!found)
                resultP.addMonom(i);
        }
        for (Monom j: p2.getMonomList()) {
            boolean found = false;
            for (Monom i: p1.getMonomList()) {
                Monom result = new Monom(0, 0);
                if (i.getExponent() == j.getExponent()) {
                    found = true;
                }
            }
            if (!found){
                j.setCoefficient(-j.getCoefficient());
                resultP.addMonom(j);
            }
        }
        resultP.arrangePolynom();
        return resultP;
    }
    public Polynom Derivation(Polynom p){
        Polynom resultP=new Polynom();
        for(Monom m:p.getMonomList()){
            Monom result = new Monom(0, 0);
            result.derivation(m);
            resultP.addMonom(result);
        }
        resultP.arrangePolynom();
        return resultP;

    }
    public Polynom Integration(Polynom p){
        Polynom resultP=new Polynom();
        for(Monom m:p.getMonomList()){
            Monom result = new Monom(0, 0);
            result.integration(m);
            resultP.addMonom(result);
        }
        resultP.arrangePolynom();
        return resultP;
    }

    public Polynom rest(Polynom p1,Polynom p2){
        Polynom restP=new Polynom();
        restP.copyPolynom(p1);
        if(p1.getDegree()<p2.getDegree() ||(p1.getDegree()==p2.getDegree() && p1.monomList.get(0).getCoefficient()<p2.monomList.get(0).getCoefficient()))
            return restP;
        while(!restP.isZero() && restP.getDegree()>=p2.getDegree()){
            Polynom t=new Polynom();
            Monom toAdd=new Monom();
            toAdd.division(restP.getMonomList().get(0),p2.getMonomList().get(0));
            t.addMonom(toAdd);
            restP=Subtraction(restP,Multiplication(t,p2));
            restP.getMonomList().remove(restP.getMonomList().get(0));
        }
        restP.arrangePolynom();
        return restP;
    }
    public Polynom cat(Polynom p1,Polynom p2){
        Polynom restP=new Polynom();
        Polynom catP=new Polynom();
        restP.copyPolynom(p1);
        if(p1.getDegree()<p2.getDegree() ||(p1.getDegree()==p2.getDegree() && p1.monomList.get(0).getCoefficient()<p2.monomList.get(0).getCoefficient()))
            return null;
        while(!restP.isZero() && restP.getDegree()>=p2.getDegree() ){
            Polynom t=new Polynom();
            Monom toAdd=new Monom();
            toAdd.division(restP.getMonomList().get(0),p2.getMonomList().get(0));
            t.addMonom(toAdd);
            catP=Addition(catP,t);
            restP=Subtraction(restP,Multiplication(t,p2));
            restP.getMonomList().remove(restP.getMonomList().get(0));
        }
        catP.arrangePolynom();
        return catP;
    }
}