package project;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        /*
        //First Polynom
        Monom monom11=new Monom(5 ,17);
        Monom monom12=new Monom(4,4);
        Monom monom13=new Monom(1,-1);
        Monom monom14=new Monom(0,5);
        Polynom testP1=new Polynom();
        testP1.addMonom(monom11);
        testP1.addMonom(monom12);
        testP1.addMonom(monom13);
        testP1.addMonom(monom14);
        testP1.arrangePolynom();
        String first=testP1.printPolynom();
        System.out.println(first);
        //Second Polynom
        Monom monom21=new Monom(2 ,1);
        Monom monom22=new Monom(1,-1);
        Monom monom23=new Monom(0,1);
        Polynom testP2=new Polynom();
        testP2.addMonom(monom21);
        testP2.addMonom(monom22);
        testP2.addMonom(monom23);
        testP2.arrangePolynom();
        String second=testP1.printPolynom();
        System.out.println(second);

        //Create result
        Polynom resultP=new Polynom();
        System.out.println("Choose the operation");
        System.out.println("For Addition press 1");
        System.out.println("For Subtraction press 2");
        System.out.println("For Multiplication press 3");
        System.out.println("For Derivation press 4");
        System.out.println("For Integration press 5");
        System.out.println("For Division press 6");
        Scanner scanner=new Scanner(System.in);
        int choose=scanner.nextInt();
        String toPrint;

        if(choose==1){
            resultP=resultP.Addition(testP1,testP2);
            resultP.arrangePolynom();
            toPrint=resultP.printPolynom();
        }
        else if(choose==2){
            resultP=resultP.Subtraction(testP1,testP2);
            resultP.arrangePolynom();
            toPrint=resultP.printPolynom();
        }
        else if(choose==3) {
            resultP=resultP.Multiplication(testP1, testP2);
            resultP.arrangePolynom();
            toPrint=resultP.printPolynom();
        }

        else if(choose==4){
            resultP=resultP.Derivation(testP1);
            resultP.arrangePolynom();
            toPrint=resultP.printPolynom();
            System.out.println(toPrint);
        }
        else if(choose==5){
            resultP=resultP.Integration(testP1);
            resultP.arrangePolynom();
            toPrint=resultP.printPolynom();
            System.out.println(toPrint);
        }
        else if(choose==6){
            resultP=resultP.rest(testP1,testP2);
            toPrint="Rest";
            toPrint+=resultP.printPolynom();
            resultP=resultP.cat(testP1,testP2);
            toPrint+="\nCat";
            toPrint+=resultP.printPolynom();
            System.out.println(toPrint);

        }

        else
            System.out.println("Invalid INPUT");

    }
    */
        Model model = new Model();
        View view = new View(model);
        Controller controller = new Controller(model, view);
        view.setVisible(true);
    }
    }


