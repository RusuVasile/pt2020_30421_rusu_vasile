package project;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class View extends JFrame {
    private Model model;

    private JTextField polynom1=new JTextField(30);
    private JTextField polynom2=new JTextField(30);
    private JTextField result=new JTextField(30);
    private JTextField result2=new JTextField(30);
    private JButton add = new JButton(  "+");
    private JButton mul = new JButton(  "*");
    private JButton der = new JButton(  "'");
    private JButton sub = new JButton(  "-");
    private JButton div = new JButton(  "/");
    private JButton integ = new JButton("∫ ");
    private JButton clear=new JButton("Clear");
    public View(Model model){
        this.model=model;
        JPanel panel=new JPanel();
        panel.setPreferredSize(new Dimension(400,300));
        FlowLayout flow=new FlowLayout();
        panel.setLayout(flow);

        panel.add(new JLabel("Polynom1"));
        panel.add(polynom1);
        panel.add(new JLabel("Polynom2"));
        panel.add(polynom2);
        panel.add(new JLabel("RESULT"));
        panel.add(result);
        panel.add(new JLabel("REST  (/)"));
        panel.add(result2);
        add.setPreferredSize(new Dimension(60,60));
        add.setFont(new Font("Arial", Font.PLAIN, 40));
        panel.add(add);
        sub.setPreferredSize(new Dimension(60,60));
        sub.setFont(new Font("Arial", Font.PLAIN, 40));
        panel.add(sub);
        mul.setPreferredSize(new Dimension(60,60));
        mul.setFont(new Font("Arial", Font.PLAIN, 40));
        panel.add(mul);
        div.setPreferredSize(new Dimension(60,60));
        div.setFont(new Font("Arial", Font.PLAIN, 40));
        panel.add(div);
        der.setPreferredSize(new Dimension(60,60));
        der.setFont(new Font("Arial", Font.PLAIN, 40));
        panel.add(der);
        integ.setPreferredSize(new Dimension(60,60));
        integ.setFont(new Font("Arial", Font.PLAIN, 40));
        panel.add(integ);
        clear.setPreferredSize(new Dimension(180,80));
        clear.setFont(new Font("Arial", Font.PLAIN, 30));
        panel.add(clear);
        panel.add(new JLabel("\nCorrect input form:17x^5+4x^4-2x^1-5x^0\n\n"));
        this.setContentPane(panel);
        this.pack();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public String getInput1() {
        return polynom1.getText();
    }

    public String getInput2() {
        return polynom2.getText();
    }

    public void setResult(String text){
        result.setText(text);
    }

    public void setResult2(String text) {
        result2.setText(text);
    }
    void reset(){
        polynom1.setText("");
        polynom2.setText("");
        result.setText("");
        result2.setText("");
    }
    void addAdditionListener(ActionListener mal) {
        add.addActionListener(mal);
    }
    void addSubtractionListener(ActionListener mal) {
        sub.addActionListener(mal);
    }
    void addMultiplicationListener(ActionListener mal) {
        mul.addActionListener(mal);
    }
    void addDivisionListener(ActionListener mal) {
        div.addActionListener(mal);
    }
    void addDerivativeListener(ActionListener mal) {
        der.addActionListener(mal);
    }
    void addIntegralListener(ActionListener mal) {
        integ.addActionListener(mal);
    }

    void addClearListener(ActionListener cal){
        clear.addActionListener(cal);
    }
}
