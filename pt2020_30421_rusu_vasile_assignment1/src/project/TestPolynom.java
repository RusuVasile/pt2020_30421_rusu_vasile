package project;
import static org.junit.Assert.*;
import org.junit.Test;
public class TestPolynom {
    public void test(){
        Polynom first = new Polynom();
        Polynom second = new Polynom();
        Polynom result=new Polynom();
        first = Controller.readInput("17x^5+4x^4-2x^1-5x^0");
        second = Controller.readInput("-2x^2+2x^1-5x^0");

        result = result.Addition(first,second);
        assertEquals("17(X^5)+4(X^4)-2(X^2)-10(X^0)", result.printPolynom());

        result =result.Subtraction(first, second);
        assertEquals("17(X^5)+4(X^4)+2(X^2)-4(X^1)", result.printPolynom());

        result = result.Multiplication(first, second);
        assertEquals("-34(X^7)+26(X^6)-77(X^5)-20(X^4)+4(X^3)+6(X^2)+25(X^0)", result.printPolynom());

        Polynom rest=new Polynom();
        Polynom cat=new Polynom();
        cat = cat.cat(first, second);
        rest=rest.rest(first,second);
        assertEquals("-8.5(X^3)-10.5(X^2)+10.75(X^1)+37(X^0)",cat.printPolynom());
        assertEquals("-22.25(X^1)+180(X^0)",rest.printPolynom());

        result= result.Derivation(first);
        assertEquals("85(X^4)+16(X^3)-2(X^0)", result.printPolynom());

        result =result.Integration(first);
        assertEquals("2.83(X^6)+0.8(X^5)-1(X^2)-5(X^1)", result.printPolynom());

    }
}
