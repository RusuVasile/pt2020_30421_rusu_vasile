package project;

import javax.crypto.spec.PSource;
import java.util.ArrayList;

///NUMBER Class
public class Monom  {
    private int exponent;
    private double coefficient;

    public Monom(int exponent, double coefficient) {
        this.exponent = exponent;
        this.coefficient = coefficient;
    }
    public Monom(Monom m){
        this.coefficient = m.coefficient;
        this.exponent = m.exponent;
    }
    public void printMonom(){
        System.out.println("Monom is:"+this.getCoefficient()+"X^"+this.getExponent());
    }
    public Monom() {
    }
    //GETTERS
    public int getExponent() {
        return exponent;
    }

    public double getCoefficient() {
        return coefficient;
    }

    //SETTERS
    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public void setExponent(int exponent) {
        this.exponent = exponent;
    }

    public void checkExponent(int exponent) {
        if (exponent < 0)
            System.out.println("Exponent should be >0");
    }

    public Monom addition(Monom monom1, Monom monom2) {
        this.coefficient=monom1.coefficient+monom2.coefficient;
        this.exponent=monom1.exponent;
        return this;
    }

    public Monom subtraction(Monom monom1, Monom monom2) {
        this.exponent=monom1.exponent;
        this.coefficient=monom1.coefficient-monom2.coefficient;
        return this;
    }
    public Monom multiplication(Monom monom1, Monom monom2) {
        this.coefficient=monom1.coefficient*monom2.coefficient;
        this.exponent=monom1.exponent+monom2.exponent;
        return this;
    }
    public Monom division(Monom monom1, Monom monom2) {
            this.coefficient=monom1.coefficient/monom2.coefficient;
            this.exponent = monom1.exponent - monom2.exponent;
            return this;
        }
    public Monom derivation(Monom monom1) {
        this.coefficient=monom1.coefficient* monom1.exponent;
        this.exponent=monom1.exponent;
        this.exponent--;
        return  this;
    }
    public Monom integration(Monom monom1) {
        this.coefficient=monom1.coefficient/ (monom1.exponent+1);
        this.exponent=monom1.exponent;
        this.exponent++;
        return this;
    }
    }
