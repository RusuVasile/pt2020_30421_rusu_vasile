package project;

public class Model {
    private Polynom first,second;
    private Polynom result,result2;
    //EMPTY INPUT AND OUTPUT POLYNOMS CONSTRUCTOR
    public Model() {
        first=new Polynom();
        second=new Polynom();
        result=new Polynom();
        result2=new Polynom();
    }
    //GETTERS
    public Polynom getFirst() {
        return first;
    }

    public Polynom getSecond() {
        return second;
    }

    public Polynom getResult() {
        return result;
    }

    public Polynom getResult2() { return result2; }

    public void setFirst(Polynom first) {
        this.first = first;
    }

    public void setSecond(Polynom second) {
        this.second = second;
    }

    public void setResult(Polynom result) {
        this.result = result;
    }
    public void setResult2(Polynom result2) {
        this.result2 = result2;
    }
    //Function to reset INPUTS AND OUTPUT

   public void clear(){
        first=new Polynom();
        second=new Polynom();
        result=new Polynom();
        result2=new Polynom();
   }
}
